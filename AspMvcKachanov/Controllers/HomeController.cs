﻿using AspMvcKachanov.Models;
using AspMvcKachanov.Providers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AspMvcKachanov.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            return View();
        }

        public async Task<ActionResult> LoadDataTable()
        {
            var users = new List<AttendanceRecord>();
            var content = await HttpRequestsProvider.Instance.GetCsvData(Constants.GetCsvDataUrl);

            if (!string.IsNullOrEmpty(content))
            {
                ParseDataProvider.Instance.ParseCsvData(content, users);
            }

            return Json(new { data = users }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}