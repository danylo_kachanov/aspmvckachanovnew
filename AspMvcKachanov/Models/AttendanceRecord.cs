﻿namespace AspMvcKachanov.Models
{
    public class AttendanceRecord
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        public string TabId { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string EntryPoint { get; set; }
        public string AccessZone { get; set; }
    }
}