﻿using System;

namespace AspMvcKachanov.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            // Handles negative ends.
            if (end < 0)
            {
                end = source.Length + end;
            }
            int len = end - start;

            // Return new array.
            T[] res = new T[len];
            for (int i = 0; i < len; i++)
            {
                res[i] = source[i + start];
            }
            return res;
        }

        public static T Slice<T>(this T[] source, int index)
        {
            if (index < 0 && index > source.Length - 1)
            {
                index = 0;
                throw new IndexOutOfRangeException("fiasko");
            }

            return source[index];
        }
    }
}